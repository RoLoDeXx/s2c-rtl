var GLOBAL_CONFIG = {
  s2cUrl: "https://survey2connect.com",
  s2cDomain: "survey2connect.com",
  exportUrl: "https://export.survey2connect.com",
  createSurveyUrl: "https://design.survey2connect.com",
  takeSurveyUrl: "https://take.survey2connect.com",
  m2cUrl: "https://m2c.survey2connect.com",
  onPremise: false,
  captchaEnabled: false,
  adLoginOnly: false
}
